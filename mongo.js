// 1. In our hotel database, create a "rooms" collection and insert a single room with the following details:
// name - single
// accomodates - 2
// price - 1000
// description - A simple room with all the basic necessities
// rooms_available - 10
// isAvailable - false

db.rooms.insertOne(
	{
		"name": "single",
		"accomodates": 2,
		"price":1000,
		"description": "A simple room with all the basic necessities",
		"rooms_available": 10,
		"isAvailable": false
	}
)

// 2. Insert multiple rooms with the following details
// name - double
// accomodates - 3
// price - 2000
// description - A room fit for a small family going on a vacation
// rooms_available - 5
// isAvailable - false

// name - queen
// accomodates - 4
// price - 4000
// description - A room with a queen sized bed perfect for a simple getaway
// rooms_available - 15
// isAvailable - false

db.rooms.insertMany([
	{
		"name": "double",
		"accomodates": 3,
		"price":2000,
		"description": "A room fit for a small family going on a vacation",
		"rooms_available": 5,
		"isAvailable": false
	},
	{
		"name": "queen",
		"accomodates": 4,
		"price":4000,
		"description": "A room with a queen sized bed perfect for a simple getaway",
		"rooms_available": 15,
		"isAvailable": false
	}
])

// 3. Find rooms with a price greater than or equal to 2000
db.rooms.find(
	{
		"price":{
			"$gte": 2000
		}
	}
)

// 4. Find a room with a price greater than or equal to 2000 and with rooms_available greater than 10
db.rooms.find(
	{
		"price":{
			"$gte": 2000
		},
		"rooms_available":{
			"$gt": 10
		}
	}
)

// 5. Update the "queen" room to have rooms_available to 0
db.rooms.updateOne(
	{
		"_id": ObjectId("6177f105d3378c6cfe0789d4")
	},
	{
		"$set": {
			"rooms_available": 0
		}
	}
)
// 6. Find all rooms with rooms_available greater than 0 and the isAvailable to false and set the availability of the rooms to "true"

db.rooms.updateMany(
	{
		"rooms_available":{
			"$gt": 0
		}
	},
	{
		$set: {
			"isAvailable": true
		}
	}
)

// 7. Delete all rooms with rooms_available equal to 0

db.rooms.deleteMany(
	{
		"rooms_available":0
	}
)

// 8. Query a room with the name "double" and project only the following fields:

// name
// accomodates
// price
// description

db.rooms.find(
	{
		"_id": ObjectId("6177f105d3378c6cfe0789d3")
	},
	{
		"name":1,
		"accomodates":1,
		"price":1,
		"description":1
	}
)

// 9. Do the same query as previously done but exclude the id.

db.rooms.find(
	{
		"_id": ObjectId("6177f105d3378c6cfe0789d3")
	},
	{
		"name":1,
		"accomodates":1,
		"price":1,
		"description":1,
		"_id":0
	}
)





